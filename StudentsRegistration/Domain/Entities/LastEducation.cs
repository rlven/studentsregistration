﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class LastEducation : Entity<int>
    {
        public string SchoolName { get; set; }
        public string UniversityName { get; set; }
        public virtual StudentsInfo StudentsInfo { get; set; }
    }
}
