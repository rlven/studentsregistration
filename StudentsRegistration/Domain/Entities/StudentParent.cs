﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class StudentParent : Entity<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Relation { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public virtual StudentsInfo StudentsInfo { get; set; }
    }
}
