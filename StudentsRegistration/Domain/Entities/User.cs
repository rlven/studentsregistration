﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Domain.Entities
{
    public class User : IdentityUser
    {
        public virtual ICollection<StudentsInfo> StudentsInfo { get; set; }
        public virtual ICollection<StudentFeedBack> StudentFeedBacks { get; set; }
    }
}
