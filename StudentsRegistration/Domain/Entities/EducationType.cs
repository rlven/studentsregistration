﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class EducationType : Entity<int>
    {
        public string Name { get; set; }
        public virtual ICollection<StudentsInfo> StudentsInfo { get; set; }
    }
}
