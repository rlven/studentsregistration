﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class StudentsInfo : Entity<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? DateBorn { get; set; }
        [ForeignKey("LastEducation")]
        public int? LastEducationId { get; set; }
        public virtual LastEducation LastEducation { get; set; }
        public string CountryName { get; set; }
        public string UniversityName { get; set; }
        public DateTime? YearOfEducationStart { get; set; }
        [ForeignKey("EducationProgramm")]
        public int? EducationProgrammId { get; set; }
        public virtual EducationProgramm EducationProgramm { get; set; }
        public string Speciality { get; set; }
        [ForeignKey("EducationType")]
        public int? EducationTypeId { get; set; }
        public virtual EducationType EducationType{ get; set; }
        [ForeignKey("StudentStatus")]
        public int? StatusId { get; set; }
        public virtual StudentStatus StudentStatus { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool? IsAcceptDataSharing { get; set; }
        [ForeignKey("StudentParent")]
        public int? ParentsContactId { get; set; }
        public virtual StudentParent StudentParent { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}
