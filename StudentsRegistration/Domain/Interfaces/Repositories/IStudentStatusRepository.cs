﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;

namespace Domain.Interfaces.Repositories
{
    public interface IStudentStatusRepository : IRepository<StudentStatus, int>
    {
    }
}
