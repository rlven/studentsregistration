﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Interfaces.Repositories
{
    public interface IStudentsInfoRepository : IRepository<StudentsInfo, int>
    {
        StudentsInfo GetStudentInfoById(string id);
    }
}
