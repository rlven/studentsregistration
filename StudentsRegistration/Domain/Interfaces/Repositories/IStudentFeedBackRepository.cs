﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Domain.Interfaces
{
    public interface IStudentFeedBackRepository : IRepository<StudentFeedBack, int>
    {
    }
}
