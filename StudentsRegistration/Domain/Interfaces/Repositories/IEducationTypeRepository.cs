﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;

namespace Domain.Interfaces.Repositories
{
    public interface IEducationTypeRepository : IRepository<EducationType, int>
    {
    }
}
