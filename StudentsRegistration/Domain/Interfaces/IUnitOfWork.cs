﻿using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUnitOfWork
    {
       IUserRepository UserRepository { get; }
       IEducationProgrammRepository EducationProgrammRepository { get; }
       IEducationTypeRepository EducationTypeRepository { get; }
       ILastEducationRepository LastEducationRepository { get;  }
       IStudentParentRepository StudentParentRepository { get; }
       IStudentsInfoRepository StudentsInfoRepository { get; }
       IStudentStatusRepository StudentStatusRepository { get; }
       IStudentFeedBackRepository StudentFeedBackRepository { get; }
        void Save();
        Task SaveAsync();
    }
}
