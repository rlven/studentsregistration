﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;

namespace Common.Models.StudentsPanelModels
{
    public class StudentsFilterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public int? EducationProgrammId { get; set; }
        public int? StudentStatusId { get; set; }
        public int? EducationTypeId { get; set; }
    }

    public class StudentsFilteredModel
    {
        public IQueryable<StudentsInfo> FilterData(IQueryable<StudentsInfo> data, StudentsFilterModel filterModel)
        {
            if (!string.IsNullOrEmpty(filterModel.FirstName))
            {
                data = data.Where(x => x.FirstName.Equals(filterModel.FirstName));
            }
            if (!string.IsNullOrEmpty(filterModel.Email))
            {
                data = data.Where(x => x.Email.Equals(filterModel.Email));
            }
            if (!string.IsNullOrEmpty(filterModel.LastName))
            {
                data = data.Where(x => x.LastName.Equals(filterModel.LastName));
            }
            if (!string.IsNullOrEmpty(filterModel.MiddleName))
            {
                data = data.Where(x => x.MiddleName.Equals(filterModel.MiddleName));
            }
            if (filterModel.EducationProgrammId != null)
            {
                data = data.Where(x => x.EducationProgrammId == filterModel.EducationProgrammId);
            }
            if (filterModel.StudentStatusId != null)
            {
                data = data.Where(x => x.StatusId == filterModel.StudentStatusId);
            }
            if (filterModel.EducationTypeId != null)
            {
                data = data.Where(x => x.EducationTypeId == filterModel.EducationTypeId);
            }

            return data;
        }
    }
}
