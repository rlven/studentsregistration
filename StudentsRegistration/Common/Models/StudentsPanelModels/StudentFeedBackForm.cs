﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.Models.StudentsPanelModels
{
    public class StudentFeedBackForm
    {
        [Required]
        public string Title { get; set; }
        [Required] 
        public string Body { get; set; }
    }
}
