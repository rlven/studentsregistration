﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Models.StudentsPanelModels
{
    public class StudentInfoExportModel
    {
        [DisplayName("Имя")]
        public string FirstName { get; set; }
        [DisplayName("Фамилия")]
        public string LastName { get; set; }
        [DisplayName("Отчество")]
        public string MiddleName { get; set; }
        [DisplayName("Дата рождения")]
        public DateTime? DateBorn { get; set; }
        [DisplayName("Страна обучения")]
        public string CountryName { get; set; }
        [DisplayName("Название учереждения")]
        public string UniversityName { get; set; }
        [DisplayName("Год поступления")]
        public DateTime? YearOfEducationStart { get; set; }
        [DisplayName("Программа обучения")]
        public string EducationProgramm { get; set; }
        [DisplayName("Специальность")]
        public string Speciality { get; set; }
        [DisplayName("Тип обучения")]
        public string EducationType { get; set; }
        [DisplayName("Статус студента")]
        public string Status { get; set; }
        [DisplayName("Контактный номер")]
        public string PhoneNumber { get; set; }
        [DisplayName("Почта")]
        public string Email { get; set; }
        [DisplayName("Согласен на обработку данных")]
        public bool? IsAcceptDataSharing { get; set; }
        [DisplayName("Номер попечителя")]
        public string ParentsContantNumber { get; set; }
        [DisplayName("Почта попечителя")]
        public string ParentsEmail { get; set; }
        [DisplayName("Отношение попечителя")]
        public string ParentRelation { get; set; }
        [DisplayName("Имя попечителя")]
        public string ParentFirstName { get; set; }
        [DisplayName("Фамилия попечителя")]
        public string ParentLastName { get; set; }
        [DisplayName("Отчество попечителя")]
        public string ParentMiddleName { get; set; }
        [DisplayName("Прошлое образование школа")]
        public string LastEducationSchoolName { get; set; }
        [DisplayName("Прошлое образование университет")]
        public string LastEducationUniversityName { get; set; }
    }
}
