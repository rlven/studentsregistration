﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.Models.StudentsPanelModels
{
    public class StudentRegistrationModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string MiddleName { get; set; }
        [Required]
        public DateTime? DateBorn { get; set; }
        public int? LastEducationId { get; set; }
        [Required]
        public string CountryName { get; set; }
        [Required]
        public string UniversityName { get; set; }
        [Required]
        public DateTime? YearOfEducationStart { get; set; }
        [Required]
        public int? EducationProgrammId { get; set; }
        [Required]
        public string Speciality { get; set; }
        [Required]
        public int? EducationTypeId { get; set; }
        [Required]
        public int? StatusId { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public bool IsAcceptDataSharing { get; set; }
        [Required]
        public string ParentsContantNumber { get; set; }
        [Required]
        public string ParentsEmail { get; set; }
        [Required]
        public string ParentRelation { get; set; }
        [Required]
        public string ParentFirstName { get; set; }
        [Required]
        public string ParentLastName { get; set; }
        [Required]
        public string ParentMiddleName { get; set; }
        [Required]
        public string LastEducationSchoolName { get; set; }
        public string LastEducationUniversityName { get; set; }

        public int Id { get; set; }

    }
}
