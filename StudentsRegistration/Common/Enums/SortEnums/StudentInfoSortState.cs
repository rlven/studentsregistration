﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Enums.SortEnums
{
    public enum StudentInfoSortState
    {
        FirstNameAsc,
        FirstNameDesc,
        LastNameAsc,
        LastNameDesc,
        MiddleNameAsc,
        MiddleNameDesc,
        DateBornAsc,
        DateBornDesc,
        CountryNameAsc,
        CountryNameDesc,
        UniversityNameAsc,
        UniversityNameDesc,
        YearOfEduStartAsc,
        YearOfEduStartDesc,
        PhoneNumberAsc,
        PhoneNumberDesc,
        EmailAsc,
        EmailDesc
    }
}
