﻿using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }
        public virtual DbSet<EducationProgramm> EducationProgramms { get; set; }
        public virtual DbSet<EducationType> EducationTypes { get; set; }
        public virtual DbSet<StudentParent> StudentParents { get; set; }
        public virtual DbSet<StudentsInfo> StudentsInfos { get; set; }
        public virtual DbSet<StudentStatus> StudentStatuses { get; set; }
        public virtual DbSet<StudentFeedBack> StudentFeedBacks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EducationProgramm>().HasData(
                new EducationProgramm() { Id = 1, Name = "Бакалавриат" },
                new EducationProgramm() { Id = 2, Name = "Магистратура" },
                new EducationProgramm() { Id = 3, Name = "Докторантура" },
                new EducationProgramm() { Id = 4, Name = "Языковая стажировка" },
                new EducationProgramm() { Id = 5, Name = "Летняя школа" },
                new EducationProgramm() { Id = 6, Name = "Академическая мобильность" });

            modelBuilder.Entity<EducationType>().HasData(
                new EducationType() { Id = 1, Name = "Бюджет" },
                new EducationType() { Id = 2, Name = "Стипендиальная программа" },
                new EducationType() { Id = 3, Name = "Контракт" });

            modelBuilder.Entity<StudentStatus>().HasData(
                new StudentStatus() { Id = 1, Name = "Зачислен" },
                new StudentStatus() { Id = 2, Name = "Переведен на следующий курс" },
                new StudentStatus() { Id = 3, Name = "Отчислен" },
                new StudentStatus() { Id = 4, Name = "Академический отпуск" });

            base.OnModelCreating(modelBuilder);
        }
    }
}
