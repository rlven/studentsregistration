﻿using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit.Text;

namespace Infrastructure.Helpers
{
    public static class MailHelper
    {
        public static IConfiguration configuration;
        public static async Task SendEmailAsync(string email, string text)
        {
            MimeMessage message = new MimeMessage();
            message.To.Add(new MailboxAddress(configuration.GetSection("OrganizationName").Value,
                email));
            message.From.Add(new MailboxAddress(configuration.GetSection("OrganizationName").Value,
                configuration.GetSection("SmtpUsername").Value));
            message.Body = new TextPart(TextFormat.Text)
            {
                Text = text
            };
            using (var emailClient = new SmtpClient())
            {
                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                await emailClient.ConnectAsync(configuration.GetSection("SmtpServerName").Value, 465, true);
                await emailClient.AuthenticateAsync(configuration.GetSection("SmtpUsername").Value, configuration.GetSection("SmtpPassword").Value);
                await emailClient.SendAsync(message);
                await emailClient.DisconnectAsync(true);
            }

        }
    }
}
