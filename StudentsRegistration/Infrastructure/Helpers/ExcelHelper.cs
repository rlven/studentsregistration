﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using OfficeOpenXml;

namespace Infrastructure.Helpers
{
    public class ExcelHelper
    {
        public ExcelWorksheet Worksheet { get; set; }

        public string HeaderRange { get; set; }

        public string UriPath { get; set; }

        private List<string> propertyNames;
        private ExcelPackage excel;

        private string path;

        public ExcelHelper(ExcelPackage excel)
        {
            propertyNames = new List<string>();
            this.excel = excel;
        }

        public void CreateExcelFile<T>(string sheetName, string folderPath, IEnumerable<T> data, string reportId)
        {
            if (!data.Any())
                throw new Exception();

            excel.Workbook.Worksheets.Add($"{sheetName}");
            Worksheet = excel.Workbook.Worksheets[$"{sheetName}"];

            SetPropertyNames(data);

            var headerRow = new List<string[]>()
            {
                propertyNames.ToArray()
            };

            HeaderRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
            Worksheet.Cells[HeaderRange].LoadFromArrays(headerRow);

            if (!Directory.Exists($"{folderPath}/Reports/{sheetName}"))
                Directory.CreateDirectory($"{folderPath}/Reports/{sheetName}");

            StyleCells();

            Worksheet.Cells[2, 1].LoadFromCollection(data);

            path = $"{folderPath}/Reports/{sheetName}/{reportId}.xlsx";

            UriPath = $"/Reports/{sheetName}/{reportId}.xlsx";
        }

        public void SaveExcelFile()
        {
            FileInfo excelFile = new FileInfo(path);
            excel.SaveAs(excelFile);
        }

        private void StyleCells()
        {
            Worksheet.Cells[HeaderRange].Style.Font.Bold = true;
            Worksheet.Cells[HeaderRange].Style.Font.Size = 14;
            Worksheet.Cells[HeaderRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);
            Worksheet.Cells["A1:O100"].AutoFitColumns();
        }

        private void SetPropertyNames<T>(IEnumerable<T> data)
        {
            var type = data.FirstOrDefault().GetType();
            var properties = type.GetProperties();

            for (int i = 0; i < properties.Length; i++)
            {
                if (properties[i].PropertyType.FullName.Contains(nameof(ICollection<T>)))
                    continue;

                if (properties[i].PropertyType.ToString().Contains(typeof(DateTime).ToString()))
                    Worksheet.Column(i + 1).Style.Numberformat.Format = "dd-MM-yyyy HH:mm";

                var atts = properties[i].GetCustomAttributes(
                    typeof(DisplayNameAttribute), true);
                if (atts.Length == 0)
                {
                    propertyNames.Add(properties[i].Name);
                }
                else
                {
                    string propName = (atts[0] as DisplayNameAttribute).DisplayName;
                    propertyNames.Add(propName);
                }
            }
        }
    }
}
