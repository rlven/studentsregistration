﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class StudentFeedBackRepository : Repository<StudentFeedBack, int>, IStudentFeedBackRepository
    {
        public StudentFeedBackRepository(AppDbContext context) : base(context)
        {
        }
    }
}
