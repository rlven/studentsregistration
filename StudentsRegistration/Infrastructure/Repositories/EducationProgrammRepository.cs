﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class EducationProgrammRepository : Repository<EducationProgramm, int>, IEducationProgrammRepository
    {
        public EducationProgrammRepository(AppDbContext context) : base(context)
        {
        }
    }
}
