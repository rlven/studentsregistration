﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class StudentStatusRepository : Repository<StudentStatus, int>, IStudentStatusRepository
    {
        public StudentStatusRepository(AppDbContext context) : base(context)
        {
        }
    }
}
