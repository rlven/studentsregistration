﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class LastEducationRepository : Repository<LastEducation, int>, ILastEducationRepository
    {
        public LastEducationRepository(AppDbContext context) : base(context)
        {
        }
    }
}
