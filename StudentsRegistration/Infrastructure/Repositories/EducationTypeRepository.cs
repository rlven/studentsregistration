﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class EducationTypeRepository : Repository<EducationType, int>, IEducationTypeRepository
    {
        public EducationTypeRepository(AppDbContext context) : base(context)
        {
        }
    }
}
