﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class StudentParentRepository : Repository<StudentParent, int>, IStudentParentRepository
    {
        public StudentParentRepository(AppDbContext context) : base(context)
        {
        }
    }
}
