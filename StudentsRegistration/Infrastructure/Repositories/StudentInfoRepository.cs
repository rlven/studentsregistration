﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class StudentInfoRepository : Repository<StudentsInfo, int>, IStudentsInfoRepository
    {
        private readonly AppDbContext context;
        public StudentInfoRepository(AppDbContext context) : base(context)
        {
            this.context = context;
        }

        public StudentsInfo GetStudentInfoById(string id)
        {
            return context.StudentsInfos.FirstOrDefault(x=>x.UserId.Equals(id));
        }
    }
}
