﻿using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext context;
        private IUserRepository userRepository;
        private IEducationProgrammRepository educationProgrammRepository;
        private IEducationTypeRepository educationTypeRepository;
        private ILastEducationRepository lastEducationRepository;
        private IStudentParentRepository studentParentRepository;
        private IStudentsInfoRepository studentsInfoRepository;
        private IStudentStatusRepository studentStatusRepository;
        private IStudentFeedBackRepository studentFeedBackRepository;

        public UnitOfWork(AppDbContext context)
        {
            this.context = context;
        }
        public IUserRepository UserRepository
        {
            get { return userRepository = new UserRepository(context); }
        }
        public IEducationProgrammRepository EducationProgrammRepository
        {
            get { return educationProgrammRepository = new EducationProgrammRepository(context); }
        }
        public IEducationTypeRepository EducationTypeRepository
        {
            get { return educationTypeRepository = new EducationTypeRepository(context); }
        }
        public ILastEducationRepository LastEducationRepository
        {
            get { return lastEducationRepository = new LastEducationRepository(context); }
        }
        public IStudentParentRepository StudentParentRepository
        {
            get { return studentParentRepository = new StudentParentRepository(context); }
        }
        public IStudentsInfoRepository StudentsInfoRepository
        {
            get { return studentsInfoRepository = new StudentInfoRepository(context); }
        }
        public IStudentStatusRepository StudentStatusRepository
        {
            get { return studentStatusRepository = new StudentStatusRepository(context); }
        }
        public IStudentFeedBackRepository StudentFeedBackRepository
        {
            get { return studentFeedBackRepository = new StudentFeedBackRepository(context); }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
