﻿using Domain.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public static class AppDbInitializer
    {
        public static async Task SeedAsync(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var dbContext = scope.ServiceProvider.GetService<AppDbContext>();

                if (!await roleManager.RoleExistsAsync("Admin"))
                {
                    IdentityRole roleAdmin = new IdentityRole("Admin");
                    await roleManager.CreateAsync(roleAdmin);
                }
                if (!await roleManager.RoleExistsAsync("User"))
                {
                    IdentityRole roleUser = new IdentityRole("User");
                    await roleManager.CreateAsync(roleUser);
                }
                if (!await roleManager.RoleExistsAsync("Edu"))
                {
                    IdentityRole roleUser = new IdentityRole("Edu");
                    await roleManager.CreateAsync(roleUser);
                }

                User admin = await userManager.FindByNameAsync("admin@admin.com");
                if (admin == null)
                {
                    var user = new User
                    {
                        UserName = "admin@admin.com",
                        Email = "admin@admin.com",
                        PhoneNumber = "1313"
                    };

                    var result = await userManager.CreateAsync(user, "admin123");

                    if (result.Succeeded)
                    {
                        await userManager.AddToRoleAsync(user, "Admin");
                    }
                }

                User edu = await userManager.FindByNameAsync("edu@edu.com");
                if (edu == null)
                {
                    var user = new User
                    {
                        UserName = "edu@edu.com",
                        Email = "edu@edu.com",
                        PhoneNumber = "1313"
                    };

                    var result = await userManager.CreateAsync(user, "edu123");

                    if (result.Succeeded)
                    {
                        await userManager.AddToRoleAsync(user, "Edu");
                        var studentInfo = new StudentsInfo() { UniversityName = "КГУСТА", UserId = user.Id };
                        dbContext.StudentsInfos.Add(studentInfo);
                        dbContext.SaveChanges();
                    }
                }

            }
        }
    }
}
