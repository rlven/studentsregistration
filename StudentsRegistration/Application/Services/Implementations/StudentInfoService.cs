﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Common.Enums.SortEnums;
using Common.Models.StudentsPanelModels;
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Hosting;
using OfficeOpenXml;

namespace Application.Services.Implementations
{
    public class StudentInfoService : IStudentInfoService
    {
        private readonly IUnitOfWork uow;
        private readonly IUserService userService;
        private readonly UserManager<User> userManager;
        private readonly IHostEnvironment env;

        public StudentInfoService(IUnitOfWork uow, IUserService userService, UserManager<User> userManager, IHostEnvironment env)
        {
            this.uow = uow;
            this.userService = userService;
            this.userManager = userManager;
            this.env = env;
        }
        public async Task RegisterStudent(StudentRegistrationModel model)
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);
            
            var studentInfo = new StudentsInfo
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                MiddleName = model.MiddleName,
                DateBorn = model.DateBorn,
                LastEducationId = model.LastEducationId,
                CountryName = model.CountryName,
                UniversityName = model.UniversityName,
                YearOfEducationStart = model.YearOfEducationStart,
                EducationProgrammId = model.EducationProgrammId,
                Speciality = model.Speciality,
                EducationTypeId = model.EducationTypeId,
                StatusId = model.StatusId,
                PhoneNumber = model.PhoneNumber,
                Email = model.Email,
                IsAcceptDataSharing = model.IsAcceptDataSharing,
                UserId = currentLogedUserId
            };
            var parentsContacts = new StudentParent()
            {
                Email = model.ParentsEmail, FirstName = model.ParentFirstName, LastName = model.ParentLastName,
                MiddleName = model.ParentMiddleName, PhoneNumber = model.ParentsContantNumber, Relation = model.ParentRelation
            };
            var lastEducation = new LastEducation()
            {
                SchoolName = model.LastEducationSchoolName,
                UniversityName = model.LastEducationUniversityName
            };
            await uow.LastEducationRepository.AddAsync(lastEducation);
            await uow.SaveAsync();
            await uow.StudentParentRepository.AddAsync(parentsContacts);
            await uow.SaveAsync();
            studentInfo.ParentsContactId = parentsContacts.Id;
            studentInfo.LastEducationId = lastEducation.Id;
            await uow.StudentsInfoRepository.AddAsync(studentInfo);
            await uow.SaveAsync();
        }

        public StudentRegistrationModel GetStudentInfo()
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);

            var userInfos = uow.StudentsInfoRepository.All.AsEnumerable();
            var userInfo = userInfos.LastOrDefault(z => z.UserId.Equals(currentLogedUserId));
            if (userInfo == null)
                return null;
            return new StudentRegistrationModel
            {
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName,
                MiddleName = userInfo.MiddleName,
                DateBorn = userInfo.DateBorn,
                LastEducationId = userInfo.LastEducationId,
                CountryName = userInfo.CountryName,
                UniversityName = userInfo.UniversityName,
                YearOfEducationStart = userInfo.YearOfEducationStart,
                EducationProgrammId = userInfo.EducationProgrammId,
                Speciality = userInfo.Speciality,
                EducationTypeId = userInfo.EducationTypeId,
                StatusId = userInfo.StatusId,
                PhoneNumber = userInfo.PhoneNumber,
                Email = userInfo.Email,
                IsAcceptDataSharing = (bool)userInfo.IsAcceptDataSharing,
                ParentsContantNumber = userInfo.StudentParent.PhoneNumber,
                ParentsEmail = userInfo.StudentParent.Email,
                ParentRelation = userInfo.StudentParent.Relation,
                ParentFirstName = userInfo.StudentParent.FirstName,
                ParentLastName = userInfo.StudentParent.LastName,
                ParentMiddleName = userInfo.StudentParent.MiddleName,
                LastEducationSchoolName = userInfo.LastEducation.SchoolName,
                LastEducationUniversityName = userInfo.LastEducation.UniversityName
            };
        }

        public async Task UpdateStudentInfo(StudentRegistrationModel model)
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);

            var userInfo = new StudentsInfo();

            userInfo.FirstName = model.FirstName;
            userInfo.LastName = model.LastName;
            userInfo.MiddleName = model.MiddleName;
            userInfo.DateBorn = model.DateBorn;
            userInfo.CountryName = model.CountryName;
            userInfo.UniversityName = model.UniversityName;
            userInfo.YearOfEducationStart = model.YearOfEducationStart;
            userInfo.EducationProgrammId = model.EducationProgrammId;
            userInfo.Speciality = model.Speciality;
            userInfo.EducationTypeId = model.EducationTypeId;
            userInfo.StatusId = model.StatusId;
            userInfo.PhoneNumber = model.PhoneNumber;
            userInfo.Email = model.Email;
            userInfo.IsAcceptDataSharing = model.IsAcceptDataSharing;
            userInfo.UserId = currentLogedUserId;

            var studentParents = new StudentParent();
            studentParents.Email = model.ParentsEmail;
            studentParents.FirstName = model.ParentFirstName;
            studentParents.LastName = model.ParentLastName;
            studentParents.MiddleName = model.ParentMiddleName;
            studentParents.PhoneNumber = model.ParentsContantNumber;
            studentParents.Relation = model.ParentRelation;
            await uow.StudentParentRepository.AddAsync(studentParents);
            await uow.SaveAsync();

            var lastEdu = new LastEducation();
            lastEdu.SchoolName = model.LastEducationSchoolName;
            lastEdu.UniversityName = model.LastEducationUniversityName;
            await uow.LastEducationRepository.AddAsync(lastEdu);
            await uow.SaveAsync();

            userInfo.ParentsContactId = studentParents.Id;
            userInfo.LastEducationId = lastEdu.Id;
            await uow.StudentsInfoRepository.AddAsync(userInfo);
            await uow.SaveAsync();
        }

        public async Task UpdateStudentInfo(StudentRegistrationModel model, int infoId)
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);
            var userInfo = new StudentsInfo();

            userInfo.FirstName = model.FirstName;
            userInfo.LastName = model.LastName;
            userInfo.MiddleName = model.MiddleName;
            userInfo.DateBorn = model.DateBorn;
            userInfo.CountryName = model.CountryName;
            userInfo.UniversityName = model.UniversityName;
            userInfo.YearOfEducationStart = model.YearOfEducationStart;
            userInfo.EducationProgrammId = model.EducationProgrammId;
            userInfo.Speciality = model.Speciality;
            userInfo.EducationTypeId = model.EducationTypeId;
            userInfo.StatusId = model.StatusId;
            userInfo.PhoneNumber = model.PhoneNumber;
            userInfo.Email = model.Email;
            userInfo.IsAcceptDataSharing = model.IsAcceptDataSharing;
            userInfo.UserId = currentLogedUserId;

            var studentParents = new StudentParent();
            studentParents.Email = model.ParentsEmail;
            studentParents.FirstName = model.ParentFirstName;
            studentParents.LastName = model.ParentLastName;
            studentParents.MiddleName = model.ParentMiddleName;
            studentParents.PhoneNumber = model.ParentsContantNumber;
            studentParents.Relation = model.ParentRelation;
            await uow.StudentParentRepository.AddAsync(studentParents);
            await uow.SaveAsync();

            var lastEdu = new LastEducation();
            lastEdu.SchoolName = model.LastEducationSchoolName;
            lastEdu.UniversityName = model.LastEducationUniversityName;
            await uow.LastEducationRepository.AddAsync(lastEdu);
            await uow.SaveAsync();

            userInfo.LastEducationId = lastEdu.Id;
            userInfo.ParentsContactId = studentParents.Id;

            await uow.StudentsInfoRepository.AddAsync(userInfo);
            await uow.SaveAsync();
        }

        public IQueryable<StudentsInfo> GetAllStudentInfo()
        {
            return uow.StudentsInfoRepository.All;
        }

        public StudentsInfo GetInfo()
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);

            var studentsInfo = uow.StudentsInfoRepository.All.AsEnumerable();
            var lastUpdateStudentInfo = studentsInfo.LastOrDefault(z => z.UserId.Equals(currentLogedUserId));

            return lastUpdateStudentInfo;
        }

        public StudentRegistrationModel GetStudentInfoById(int id)
        {
            var userInfo = uow.StudentsInfoRepository.GetById(id);
            return new StudentRegistrationModel
            {
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName,
                MiddleName = userInfo.MiddleName,
                DateBorn = userInfo.DateBorn,
                LastEducationId = userInfo.LastEducationId,
                CountryName = userInfo.CountryName,
                UniversityName = userInfo.UniversityName,
                YearOfEducationStart = userInfo.YearOfEducationStart,
                EducationProgrammId = userInfo.EducationProgrammId,
                Speciality = userInfo.Speciality,
                EducationTypeId = userInfo.EducationTypeId,
                StatusId = userInfo.StatusId,
                PhoneNumber = userInfo.PhoneNumber,
                Email = userInfo.Email,
                IsAcceptDataSharing = (bool)userInfo.IsAcceptDataSharing,
                ParentsContantNumber = userInfo.StudentParent.PhoneNumber,
                ParentsEmail = userInfo.StudentParent.Email,
                ParentRelation = userInfo.StudentParent.Relation,
                ParentFirstName = userInfo.StudentParent.FirstName,
                ParentLastName = userInfo.StudentParent.LastName,
                ParentMiddleName = userInfo.StudentParent.MiddleName,
                LastEducationSchoolName = userInfo.LastEducation.SchoolName,
                LastEducationUniversityName = userInfo.LastEducation.UniversityName
            };
        }

        public async Task<bool> IsStudentInfoExist()
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);

            return await uow.StudentsInfoRepository.All.FirstOrDefaultAsync(x => x.UserId.Equals(currentLogedUserId)) != null;
        }

        public string ExportStudentInfoExcel(IEnumerable<StudentsInfo> data, string reportId)
        {
            var studentInfoExcelData = data.Select(x => new StudentInfoExportModel()
            {
                FirstName = x.FirstName != null ? x.FirstName : "FirstName",
                LastName = x.LastName != null ? x.LastName : "LastName",
                MiddleName = x.MiddleName != null ? x.MiddleName : "MiddleName",
                DateBorn = x.DateBorn != null ? x.DateBorn : new DateTime(123),
                CountryName = x.CountryName != null ? x.CountryName : "CountryName",
                UniversityName = x.UniversityName != null ? x.UniversityName : "UniversityName",
                YearOfEducationStart = x.YearOfEducationStart != null ? x.YearOfEducationStart : new DateTime(123),
                EducationProgramm = x.EducationProgramm != null ? x.EducationProgramm.Name : "EducationProgramm",
                Speciality = x.Speciality != null ? x.Speciality : "Speciality",
                EducationType = x.EducationType != null ? x.EducationType.Name : "EducationType",
                Status = x.StudentStatus != null ? x.StudentStatus.Name : "StudentStatus",
                PhoneNumber = x.PhoneNumber != null ? x.PhoneNumber : "PhoneNumber",
                Email = x.Email != null ? x.Email : "Email",
                IsAcceptDataSharing = x.IsAcceptDataSharing != null ? x.IsAcceptDataSharing : false,
                ParentsContantNumber = x.StudentParent != null ? x.StudentParent.PhoneNumber : "ParentsContantNumber",
                ParentsEmail = x.StudentParent != null ? x.StudentParent.Email : "ParentsEmail",
                ParentRelation = x.StudentParent != null ? x.StudentParent.Relation : "ParentRelation",
                ParentFirstName = x.StudentParent != null ? x.StudentParent.FirstName : "ParentFirstName",
                ParentLastName = x.StudentParent != null ? x.StudentParent.LastName : "ParentLastName",
                ParentMiddleName = x.StudentParent != null ? x.StudentParent.MiddleName : "ParentMiddleName",
                LastEducationSchoolName = x.LastEducation != null ? x.LastEducation.SchoolName : "LastEducationSchoolName",
                LastEducationUniversityName = x.LastEducation != null ? x.LastEducation.UniversityName : "LastEducationUniversityName"
            });

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage package = new ExcelPackage();
            ExcelHelper helper = new ExcelHelper(package);
            helper.CreateExcelFile("StudentInfosReport",env.ContentRootPath+"/wwwroot", studentInfoExcelData, reportId);
            helper.Worksheet.Cells[helper.HeaderRange].Style.Font.Size = 22;
            helper.SaveExcelFile();
            return helper.UriPath;
        }

        public List<StudentsInfo> SortData(IQueryable<StudentsInfo> infos, StudentInfoSortState sortOrder)
        {
            infos = sortOrder switch
                {
                StudentInfoSortState.FirstNameDesc => infos.OrderByDescending(s => s.FirstName),
                StudentInfoSortState.LastNameAsc => infos.OrderBy(s => s.LastName),
                StudentInfoSortState.LastNameDesc => infos.OrderByDescending(s => s.LastName),
                StudentInfoSortState.MiddleNameAsc => infos.OrderBy(s => s.MiddleName),
                StudentInfoSortState.MiddleNameDesc => infos.OrderByDescending(s => s.MiddleName),
                StudentInfoSortState.DateBornAsc => infos.OrderBy(s => s.DateBorn),
                StudentInfoSortState.DateBornDesc => infos.OrderByDescending(s => s.DateBorn),
                StudentInfoSortState.CountryNameAsc => infos.OrderBy(s => s.CountryName),
                StudentInfoSortState.CountryNameDesc => infos.OrderByDescending(s => s.CountryName),
                StudentInfoSortState.UniversityNameAsc => infos.OrderBy(s => s.UniversityName),
                StudentInfoSortState.UniversityNameDesc => infos.OrderByDescending(s => s.UniversityName),
                StudentInfoSortState.YearOfEduStartAsc => infos.OrderBy(s => s.YearOfEducationStart),
                StudentInfoSortState.YearOfEduStartDesc => infos.OrderByDescending(s => s.YearOfEducationStart),
                StudentInfoSortState.PhoneNumberAsc => infos.OrderBy(s => s.PhoneNumber),
                StudentInfoSortState.PhoneNumberDesc => infos.OrderByDescending(s => s.PhoneNumber),
                StudentInfoSortState.EmailAsc => infos.OrderBy(s => s.Email),
                StudentInfoSortState.EmailDesc => infos.OrderByDescending(s => s.Email),
                _ => infos.OrderBy(s => s.FirstName),
                };
            return infos.ToList();
        }
    }
}
