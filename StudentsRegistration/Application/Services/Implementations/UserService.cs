﻿using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Application.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork uow;
        private readonly UserManager<User> userManager;
        private readonly IHttpContextAccessor accessor;
        public UserService(IUnitOfWork uow, UserManager<User> userManager, IHttpContextAccessor accessor)
        {
            this.uow = uow;
            this.userManager = userManager;
            this.accessor = accessor;
        }

        public async Task DeleteUserAsync(string id)
        {
            var user = await uow.UserRepository.GetByIdAsync(id);
            var result = await userManager.DeleteAsync(user);
            if (!result.Succeeded)
                throw new Exception();
        }



        public IEnumerable<User> GetUsers()
        {
            return uow.UserRepository.All.AsEnumerable();
        }

        public async Task<User> GetUserAsync(string id)
        {
            return await userManager.FindByIdAsync(id);
        }
        public ClaimsPrincipal GetUserClaims()
        {
            return accessor?.HttpContext?.User as ClaimsPrincipal;
        }
    }
}
