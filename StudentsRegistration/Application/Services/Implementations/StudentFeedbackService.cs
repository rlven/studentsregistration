﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Common.Models.StudentsPanelModels;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Application.Services.Implementations
{
    public class StudentFeedbackService : IStudentFeedbackService
    {
        private readonly IUnitOfWork uow;
        private readonly IUserService userService;
        private readonly UserManager<User> userManager;

        public StudentFeedbackService(IUnitOfWork uow, IUserService userService, UserManager<User> userManager)
        {
            this.uow = uow;
            this.userService = userService;
            this.userManager = userManager;
        }
        public async Task SendFeedbackForm(StudentFeedBackForm form)
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);
            var studentFeedBack = new StudentFeedBack()
            {
                Title = form.Title,
                Body = form.Body,
                UserId = currentLogedUserId
            };

            await uow.StudentFeedBackRepository.AddAsync(studentFeedBack);
            await uow.SaveAsync();
            //MailHelper.SendEmailAsync()
        }

        public IEnumerable<StudentFeedBack> GetAllFeedBacks()
        {
            return uow.StudentFeedBackRepository.All;
        }

        public IEnumerable<StudentFeedBack> GetAllEduFeedbacks() 
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);
            var currentUserInfo = uow.StudentsInfoRepository.All.FirstOrDefault(z => z.UserId.Equals(currentLogedUserId));

            var eduFeedbacks = uow.StudentFeedBackRepository.All.Where(x => x.User.StudentsInfo.Any(z=>z.UniversityName.Equals(currentUserInfo.UniversityName)));
            return eduFeedbacks;
        }
    }
}
