﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Application.Services.Interfaces;

namespace Application.Services.Implementations
{
    public class ValidateService : IValidateService
    {
        public bool ValidatePhone(string phone)
        {
            Regex rg = new Regex("^[0-9]{12}$");
            return rg.IsMatch(phone);
        }
    }
}
