﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums.SortEnums;
using Common.Models.StudentsPanelModels;
using Domain.Entities;

namespace Application.Services.Interfaces
{
    public interface IStudentInfoService
    {
        Task RegisterStudent(StudentRegistrationModel model);
        StudentRegistrationModel GetStudentInfo();
        Task UpdateStudentInfo(StudentRegistrationModel model);
        Task UpdateStudentInfo(StudentRegistrationModel model, int id);
        IQueryable<StudentsInfo> GetAllStudentInfo();
        StudentRegistrationModel GetStudentInfoById(int id);
        Task<bool>IsStudentInfoExist();
        string ExportStudentInfoExcel(IEnumerable<StudentsInfo> data, string reportId);
        List<StudentsInfo> SortData(IQueryable<StudentsInfo> infos, StudentInfoSortState sortOrder);
        StudentsInfo GetInfo();
    }
}
