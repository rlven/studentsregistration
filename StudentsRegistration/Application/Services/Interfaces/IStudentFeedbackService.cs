﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Common.Models.StudentsPanelModels;
using Domain.Entities;

namespace Application.Services.Interfaces
{
    public interface IStudentFeedbackService
    {
        Task SendFeedbackForm(StudentFeedBackForm form);
        IEnumerable<StudentFeedBack> GetAllFeedBacks();
        IEnumerable<StudentFeedBack> GetAllEduFeedbacks();
    }
}
