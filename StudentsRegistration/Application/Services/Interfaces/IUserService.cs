﻿
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IUserService
    {
        IEnumerable<User> GetUsers();
        Task DeleteUserAsync(string id);
        Task<User> GetUserAsync(string id);
        ClaimsPrincipal GetUserClaims();
    }
}
