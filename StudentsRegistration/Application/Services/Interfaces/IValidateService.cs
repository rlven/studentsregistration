﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IValidateService
    {
        bool ValidatePhone(string phone);
    }
}
