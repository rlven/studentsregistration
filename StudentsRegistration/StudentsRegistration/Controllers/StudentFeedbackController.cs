﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Common.Models.StudentsPanelModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace StudentsRegistrationPanel.Controllers
{
    public class StudentFeedbackController : Controller
    {
        private readonly IStudentFeedbackService studentFeedbackService;

        public StudentFeedbackController(IStudentFeedbackService studentFeedbackService)
        {
            this.studentFeedbackService = studentFeedbackService;
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> CreateStudentFeedBack(StudentFeedBackForm form)
        {
            if (ModelState.IsValid)
            {
                await studentFeedbackService.SendFeedbackForm(form);
                return Json(new { success = true });
            }

            return Json(new { success = false });
        }

        [Authorize(Roles = "Admin, Edu")]
        [HttpGet]
        public IActionResult GetAllStudentFeedbacks()
        {
            if (User.IsInRole("Edu")) 
            {
                var eduFeedbacks = studentFeedbackService.GetAllEduFeedbacks();
                return View(eduFeedbacks);
            }

            var feedbacks = studentFeedbackService.GetAllFeedBacks();
            return View(feedbacks);
        }
    }
}