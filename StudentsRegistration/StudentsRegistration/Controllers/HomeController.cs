﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FieldOfDreams.Models;
using Microsoft.AspNetCore.Authorization;

namespace FieldOfDreams.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IStudentInfoService _studentInfoService;

        public HomeController(ILogger<HomeController> logger, IStudentInfoService studentInfoService)
        {
            _logger = logger;
            _studentInfoService = studentInfoService;
        }

        public IActionResult Index()
        {
            if (User.IsInRole("User"))
            {
                var currentStudentInfo = _studentInfoService.GetInfo();
                if (currentStudentInfo == null)
                {
                    return View();
                }

                return View("StudentCard", currentStudentInfo);
            }

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
