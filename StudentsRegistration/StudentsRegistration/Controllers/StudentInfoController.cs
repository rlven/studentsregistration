﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Common.Models.StudentsPanelModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace StudentsRegistrationPanel.Controllers
{
    public class StudentInfoController : Controller
    {
        private readonly IStudentInfoService studentInfoService;

        public StudentInfoController(IStudentInfoService studentInfoService)
        {
            this.studentInfoService = studentInfoService;
        }

        [Authorize(Roles = "Admin, Edu")]
        [HttpPost]
        public string GetStudentsReportExcel()
        {
            if (User.IsInRole("Edu")) 
            {
                var studentsInfoEdu = studentInfoService.GetAllStudentInfo();
                var currentInfo = studentInfoService.GetInfo();
                studentsInfoEdu = studentsInfoEdu.Where(x => x.UniversityName.Equals(currentInfo.UniversityName) && !string.IsNullOrEmpty(x.FirstName));

                var resultEdu = studentInfoService.ExportStudentInfoExcel(studentsInfoEdu, Guid.NewGuid().ToString());
                return resultEdu;
            }
            var studentsInfo = studentInfoService.GetAllStudentInfo().Where(x => !string.IsNullOrEmpty(x.FirstName));

            var result = studentInfoService.ExportStudentInfoExcel(studentsInfo, Guid.NewGuid().ToString());
            return result;
        }
    }
}