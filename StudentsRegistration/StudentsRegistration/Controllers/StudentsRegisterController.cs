﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Common.Enums.SortEnums;
using Common.Models.StudentsPanelModels;
using Domain.Entities;
using Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace StudentsRegistrationPanel.Controllers
{
    public class StudentsRegisterController : Controller
    {
        private readonly IStudentInfoService studentInfoService;
        private readonly IValidateService validateService;
        private AppDbContext context;

        public StudentsRegisterController(IStudentInfoService studentInfoService, AppDbContext context, IValidateService validateService)
        {
            this.studentInfoService = studentInfoService;
            this.context = context;
            this.validateService = validateService;
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public async Task<IActionResult> RegisterStudent()
        {
            if (await studentInfoService.IsStudentInfoExist())
            {
                return View("AlreadyExist");
            }

            AddViewBags();
            return View();
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> RegisterStudent(StudentRegistrationModel model)
        {
            ValidateModel(model);

            if (ModelState.IsValid)
            {
                await studentInfoService.RegisterStudent(model);
                return RedirectToAction("Index", "Home");
            }

            AddViewBags();
            return View(model);
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public IActionResult Edit()
        {
            AddViewBags();

            var studentInfo = studentInfoService.GetStudentInfo();
            if (studentInfo == null)
            {
                return View("NotFound");
            }

            return View(studentInfo);
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> Edit(StudentRegistrationModel model)
        {
            ValidateModel(model);

            if (ModelState.IsValid)
            {
                await studentInfoService.UpdateStudentInfo(model);

                return RedirectToAction("Index", "Home");
            }
            AddViewBags();
            return View(model);
        }

        [Authorize(Roles = "Admin, Edu")]
        [HttpGet]
        public IActionResult GetAllStudentInfos(StudentsFilterModel filter, StudentInfoSortState sortOrder = StudentInfoSortState.FirstNameAsc)
        {
            var filterModel = new StudentsFilteredModel();
            var infos = studentInfoService.GetAllStudentInfo().Where(x=>!string.IsNullOrEmpty(x.FirstName));

            if (User.IsInRole("Edu"))
            {
                var currentInfo = studentInfoService.GetInfo();
                infos = infos.Where(x => x.UniversityName.Equals(currentInfo.UniversityName) && !string.IsNullOrEmpty(x.FirstName));
            }
            
            ViewData["FirstNameSort"] = sortOrder == StudentInfoSortState.FirstNameAsc ? StudentInfoSortState.FirstNameDesc : StudentInfoSortState.FirstNameAsc;
            ViewData["LastNameSort"] = sortOrder == StudentInfoSortState.LastNameAsc ? StudentInfoSortState.LastNameDesc : StudentInfoSortState.LastNameAsc;
            ViewData["MiddleNameSort"] = sortOrder == StudentInfoSortState.MiddleNameAsc ? StudentInfoSortState.MiddleNameDesc : StudentInfoSortState.MiddleNameAsc;
            ViewData["DateBornSort"] = sortOrder == StudentInfoSortState.DateBornAsc ? StudentInfoSortState.DateBornDesc : StudentInfoSortState.DateBornAsc;
            ViewData["CountryNameSort"] = sortOrder == StudentInfoSortState.CountryNameAsc ? StudentInfoSortState.CountryNameDesc : StudentInfoSortState.CountryNameAsc;
            ViewData["UniversityNameSort"] = sortOrder == StudentInfoSortState.UniversityNameAsc ? StudentInfoSortState.UniversityNameDesc : StudentInfoSortState.UniversityNameAsc;
            ViewData["YearOfEduStartSort"] = sortOrder == StudentInfoSortState.YearOfEduStartAsc ? StudentInfoSortState.YearOfEduStartDesc : StudentInfoSortState.YearOfEduStartAsc;
            ViewData["PhoneNumberSort"] = sortOrder == StudentInfoSortState.PhoneNumberAsc ? StudentInfoSortState.PhoneNumberDesc : StudentInfoSortState.PhoneNumberAsc;
            ViewData["EmailSort"] = sortOrder == StudentInfoSortState.EmailAsc ? StudentInfoSortState.EmailDesc : StudentInfoSortState.EmailAsc;

            infos = filterModel.FilterData(infos, filter);
            var sortedInfos = studentInfoService.SortData(infos, sortOrder);
            AddViewBags();

            return View(sortedInfos.AsEnumerable());
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult EditStudentInfo(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AddViewBags();
            ViewBag.StudentInfoId = (int)id;
            var studentInfo = studentInfoService.GetStudentInfoById((int)id);
            return View("EditStudent", studentInfo);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> EditStudentInfo(StudentRegistrationModel model)
        {
            ValidateModel(model);

            if (ModelState.IsValid)
            {
                await studentInfoService.UpdateStudentInfo(model, model.Id);
                return RedirectToAction(nameof(GetAllStudentInfos));
            }

            AddViewBags();
            ViewBag.StudentInfoId = model.Id;
            return View("EditStudent", model);
        }

        private void ValidateModel(StudentRegistrationModel model)
        {
            if (string.IsNullOrEmpty(model.PhoneNumber) || !validateService.ValidatePhone(model.PhoneNumber))
            {
                ModelState.AddModelError("PhoneNumber", "Неверный формат номера");
            }
            if (string.IsNullOrEmpty(model.ParentsContantNumber) || !validateService.ValidatePhone(model.ParentsContantNumber))
            {
                ModelState.AddModelError("ParentsContantNumber", "Неверный формат номера");
            }
        }

        private void AddViewBags()
        {
            ViewBag.EducationProgramms = new SelectList(context.EducationProgramms, "Id", "Name");
            ViewBag.EducationTypes = new SelectList(context.EducationTypes, "Id", "Name");
            ViewBag.StudentStatuses = new SelectList(context.StudentStatuses, "Id", "Name");
        }
    }
}